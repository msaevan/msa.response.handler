﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MSA.Response.Handler
{
    /// <summary>
    /// 
    /// </summary>
    public class Utility
    {   
        /// <summary>
        /// 
        /// </summary>
        public enum ResponseStatus
        {
            OK = 200, // Success
            CREATED = 201, //
            ACCEPTED = 202, // 
            NO_CONTENT = 204, //  Decline
            MOVED_PERMANENTLY = 301, //
            FOUND = 302, // Found
            SEE_OTHER = 303, //
            NOT_MODIFIED = 304, //
            TEMPORARY_REDIRECT = 305, //
            BAD_REQUEST = 400, //  Failed
            UNAUTHORIZED = 401, //
            FORBIDDEN = 403, //
            NOT_FOUND = 404, //
            METHOD_NOT_ALLOWED = 405, //
            NOT_ACCEPTABLE = 406, // Not Acceptable
            PRECONDITION_FAILED = 412, //
            UNSUPPORTED_MEDIA_TYPE = 415, //
            INTERNAL_SERVER_ERROR = 500, //
            NOT_IMPLEMENTED = 501 //
        }
    }
}
