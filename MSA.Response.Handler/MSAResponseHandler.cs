﻿using System;
using System.Collections.Generic;
using System.Text;
using static MSA.Response.Handler.Utility;

namespace MSA.Response.Handler
{
    /// <summary>
    /// 
    /// </summary>
    public class MSAResponseHandler
    {
        /// <summary>
        /// Return specific status codes, i.e. OK = 200, BAD_REQUEST = 400, UNAUTHORIZED = 401 
        /// </summary>
        public ResponseStatus Status { get; set; }
        /// <summary>
        /// Return a specific message throwing
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Return a dynamic data object
        /// </summary>
        public dynamic DataObject { get; set; }
    }
}
